package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;

public class Practica8y9 extends ApplicationAdapter implements InputProcessor, Screen {
	SpriteBatch batch;
	Texture img;
	OrthographicCamera camera;
	TiledMapRenderer tiledMapRenderer;
	TiledMap tiledMap;
	private ParticleEffect effect;


	@Override
	public void create () {

		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();

		camera = new OrthographicCamera();
		camera.setToOrtho(false,w,h);
		camera.update();
		tiledMap = new TmxMapLoader().load("Map.tmx");

		MapProperties prop = tiledMap.getProperties();

		int mapWidth = prop.get("width", Integer.class);

		int mapHeight = prop.get("height", Integer.class);

		int tilePixelWidth = prop.get("tilewidth", Integer.class);

		int tilePixelHeight = prop.get("tileheight", Integer.class);

		int mapPixelWidth = mapWidth * tilePixelWidth;

		int mapPixelHeight = mapHeight * tilePixelHeight;
		tiledMapRenderer = new OrthogonalTiledMapRenderer(tiledMap);
		Gdx.input.setInputProcessor(this);

		batch = new SpriteBatch();
		effect = new ParticleEffect();
		effect.load(Gdx.files.internal("Particles/Flame.p"),Gdx.files.internal("Particles"));

		effect.start();

	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		camera.update();
		tiledMapRenderer.setView(camera);
		tiledMapRenderer.render();

		batch.begin();
		effect.draw(batch,Gdx.graphics.getDeltaTime());
		batch.end();
	}

	@Override
	public void show() {

	}

	@Override
	public void render(float delta) {

	}

	@Override
	public void hide() {

	}

	@Override
	public void dispose () {
		batch.dispose();

	}

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		if(keycode == Input.Keys.LEFT)
			camera.translate(-32,0);
		if(keycode == Input.Keys.RIGHT)
			camera.translate(32,0);
		if(keycode == Input.Keys.UP)
			camera.translate(0,32);
		if(keycode == Input.Keys.DOWN)
			camera.translate(0,-32);
		if(keycode == Input.Keys.NUM_1)
			tiledMap.getLayers().get(0).setVisible(
					!tiledMap.getLayers().get(0).isVisible());
		if(keycode == Input.Keys.NUM_2)
			tiledMap.getLayers().get(1).setVisible(
					!tiledMap.getLayers().get(1).isVisible());
		if(keycode == Input.Keys.NUM_3)
			tiledMap.getLayers().get(2).setVisible(
					!tiledMap.getLayers().get(2).isVisible());
		if(keycode == Input.Keys.NUM_4)
			effect.scaleEffect(0.5f);
		if(keycode == Input.Keys.NUM_5)
			effect.scaleEffect(1.5f);

		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {

		effect.setPosition(screenX,Gdx.graphics.getHeight()-screenY);
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
}
